<?php

class Conexion{

	private $server;
	private $user;
	private $pass;
	private $database;
	private $charset;

	public function __construct(){

		$this->server= 'localhost';
		$this->user = 'root';
		$this->pass = '';
		$this->database = 'encuesta';
		$this->charset = 'utf8mb4';
	}

	public function connect(){

		try{

			$conn = "mysql:=host". $this->server .";dbname=". $this->database ."; charset=". $this->charset;
			$opotion = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false];

			$pdo = new PDO($conn, $this->user, $this->pass, $opotion);

			return $pdo;

		}catch(PDOException $e){
			print_r("Error connection". $e->getMessage());

		}
	}


}

