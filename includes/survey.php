<?php

	include_once 'conexion.php';

	class Survey extends Conexion{

		private $totalVotes;
		private $optionSelected;


		//Metodo Setter
		public function setOptionSelected($option){
			$this->optionSelected = $option;
		}

		//Metodo Getter
		public function getOpctionSelected(){
			return $this->optionSelected;
		}


		//Guarda el voto en la DB
		public function vote(){
			$query = $this->connect()->prepare('UPDATE lenguajes SET votos=votos+1 WHERE opcion = :opcion');
			$query->execute(['opcion' => $this->optionSelected]);
		}

		//Obtiene los votos
		public function showResults(){
			return $this->connect()->query('SELECT * FROM lenguajes');
		}

		//Cuenta el numero total de los votos en la tabla
		public function getTotalVotes(){
			$query = $this->connect()->query('SELECT SUM(votos) AS votos_totales FROM lenguajes');
			$this->totalVotes = $query->fetch(PDO::FETCH_OBJ)->votos_totales;

			return $this->totalVotes;
		}

		//Calcula los porcentajes de cada opcion
		public function getPercentageVotes($votes){
			return round(($votes / $this->totalVotes) * 100, 0);
		}
	}

 ?>