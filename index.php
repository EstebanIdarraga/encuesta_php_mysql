
<?php include_once 'includes/survey.php'; ?>



<!DOCTYPE html>
<html>
<head>
	<title>Encuenta</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<!-- Col 1 -->
			<div class="col-4"></div>

			<!-- Col 2-->
			<div class="col-4 mt-5">
				<div class="card card-body">
					<h3 class="text-center"> Lenguajes de Programacion</h3>

					<form class="mt-4" action="#" method="POST">

						<?php
							$survey = new Survey();
							$showResult = false;

							if(isset($_POST['lenguaje'])){

								$showResult = true;
								$survey->setOptionSelected($_POST['lenguaje']);
								$survey->vote();
							}


						 	if($showResult){
						 		$resultado = $survey->showResults();

						 		echo '<h2>'. $survey->getTotalVotes() .' Votos totales</h2>';


						 		foreach($resultado as $result){
						 			$porcentaje = $survey->getPercentageVotes($result['votos']);

						 			include'vistas/vistaResultado.php';
						 		}

						 		echo '<button class="btn btn-danger btn-block" name="volver"> Volver</button>';

						 		if(isset($_POST['volver'])){
						 			$showResult = false;
						 		}
						 	}else{

						 		include 'vistas/vistaVotacion.php';
						 	}

						  ?>

					</form>
				</div>
			</div>





			<!-- Col 3-->
			<div class="col-4"></div>
		</div>
	</div>

</body>
</html>